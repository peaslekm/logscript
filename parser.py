#!/usr/bin/env python


"""A Simple log parser for weblogs"""

import fileinput  #great way to not worry about the filename
import time #get current date
from datetime import datetime


def getEntries():
	lines = 0
	for line in fileinput.input():
		lines = lines + 1
	return lines

def getIPs():
	ips = []
	iphits = []
	counter = 0
	for line in fileinput.input():
		formatedIp = line.split('-', 1)[0] #from http://stackoverflow.com/questions/3762420/returning-all-characters-before-the-first-underscore
		ips.append(formatedIp)
	return ips

def most_common_mine(Ips):
	sorted_Ips = Ips
	sorted_Ips.sort()
	ip_set = []
	ip_count = [0]*len(Ips)
	i = 0
	j = 0
	ip_count[j] = ip_count[j] + 1
	for Ip in sorted_Ips:
#		print ip_count
#		print ip_set

		if Ip not in ip_set:
			ip_set.append(Ips[i])
			j+=1
		i+=1
		ip_count[j] = ip_count[j] + 1

	return ip_set,ip_count


def normalize_Ips(IPs): #adds whitespace to IPs less than 3 numbers long
	for x in range(0,len(IPs)):
		while len(IPs[x]) < 16:
			IPs[x]+=" "
	return IPs


def find_between( s, first, last ): #from http://stackoverflow.com/questions/3368969/find-string-between-two-substrings
    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )
        return s[start:end]
    except ValueError:
        return

def dates():
	date = []
	for line in fileinput.input():
		if line:
			date.append(find_between(line,'[',' '))
	return date

def todays_date_log_format():
	curr_time = time.strftime(":%H:%M:%S")
	curr_date = time.strftime("%d/%b/%Y")
	together = curr_date+curr_time
	return together

def get_date_difference(past):
	relevent_dates = []
	present = todays_date_log_format()
	present = datetime.strptime(present, "%d/%b/%Y:%H:%M:%S")
	for x in range(0,len(past)):
		past_formatted = datetime.strptime(past[x], "%d/%b/%Y:%H:%M:%S")
		if len((str(abs((past_formatted - present).days)))) == 1:
			relevent_dates.append("0" + str(abs((past_formatted - present).days)))
			#lazy way to handle empty line
		else:
			relevent_dates.append(str(abs((past_formatted - present).days)))
	return relevent_dates

def hits_and_miss():
	real = ["Mozilla", "Chrome", "Mac", "Safari"] #Probably a  much better way but this woks
	bot_or_not = []
	for line in fileinput.input():
		if any(s in line for s in real) and "404" not in line:
			bot_or_not.append(1)
		else:
			bot_or_not.append(0)
	return bot_or_not

def OS_of_requests():
	Os = []
	browsers = []
	windows,linux,Mac,firefox,safari,chrome = 0,0,0,0,0,0
	for line in fileinput.input():
		if "Windows" in line:
			windows+=1
		if "Mac" in line:
			Mac+=1
		if "Linux" in line:
			linux+=1
		if "Mozilla" in line:
			firefox+=1
		if "Safari" in line:
			safari+=1
		if "Chrome" in line:
			chrome +=1
	Os.append(windows)
	Os.append(Mac)
	Os.append(linux)
	browsers.append(firefox)
	browsers.append(safari)
	browsers.append(chrome)
	return Os,browsers
def main():

	times = get_date_difference(dates())
	num_of_Ips = getEntries()
	bot_or_not = hits_and_miss()
	Ips_most_common,ip_count = most_common_mine(normalize_Ips(getIPs()))
	Ips = normalize_Ips(getIPs())

	Os_counts,browser_count = OS_of_requests()
	OS = ["Windows", "OSX", "Linux"]
	browsers = ["Firefox", "Chrome", "Safari"]

	ip_count.pop(0)
	ip_count, Ips_most_common = zip(*sorted(zip(ip_count, Ips_most_common)))		#from http://stackoverflow.com/questions/9764298/is-it-possible-to-sort-two-listswhich-reference-each-other-in-the-exact-same-w


	print "Total Addresses: %s  in the past day(s)\n" %num_of_Ips

	print "Browser distribution : \n"
	for x in range(0,len(browsers)):
		print browsers[x] + ": " + str(browser_count[x])

	print "\nOS distribution: \n"
	for x in range(0,len(OS)):
		print OS[x] + ": " + str(Os_counts[x])

	print "\nTop hits: \n"

	i = len(ip_count)
	for x in reversed(Ips_most_common): #Really need to rewrite when finished
		if i > len(ip_count) - 5:
			print x + " - %s" %ip_count[i-1]
			i-=1
			OS_of_requests()
	print "\nFull Log list: \n"

	for x in range(len(Ips)):
		if bot_or_not[x]:
	 		print Ips[x] + " - " + str(times[x]) + " Day(s) ago - " +"Real Request"
	 	else:
	 	 	print Ips[x] + " - " + str(times[x]) + " Day(s) ago - " +"Bot"


if __name__ == "__main__":
    main()